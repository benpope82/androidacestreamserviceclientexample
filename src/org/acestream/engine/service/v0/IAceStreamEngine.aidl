/**
 * Copyright (C) 2013, ACEStream. All rights reserved.
 */

package org.acestream.engine.service.v0;

import org.acestream.engine.service.v0.IAceStreamEngineCallback;

/**
 * AceStream engine service interface
 */
interface IAceStreamEngine {

	/**
	 * Command to the service to register a client.
	 */
	void registerCallback(IAceStreamEngineCallback cb);
	
	/**
	 * Command to the service to unregister a client.
	 */
	void unregisterCallback(IAceStreamEngineCallback cb);

	/**
	 * Command to the service to start engine.
	 */
	void startEngine();

}