package org.acestream.engine.service.example.client;

import android.content.Context;
import android.os.Handler;

public abstract class BaseClient {
	
	private Context mContext;
	private Handler mHandler;
	
	public BaseClient(Context context) {
		mContext = context;
	}
	
	public void setHandler(Handler handler) {
		mHandler = handler;
	}
	
	public final Context getContext() {
		return mContext;
	}
	
	public final Handler getHandler() {
		return mHandler;
	}
	
	public abstract void bind();
	
	public abstract void unbind();

}
