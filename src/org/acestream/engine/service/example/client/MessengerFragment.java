package org.acestream.engine.service.example.client;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class MessengerFragment extends BaseFragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_mes, container, false);

		initServiceClient(new MessengerClient(getActivity().getApplicationContext()));
		initView(view);

		setRetainInstance(true);		
		return view;
	}
}
