package org.acestream.engine.service.example.client;

import org.acestream.engine.service.v0.AceStreamEngineMessages;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.widget.Toast;

public class MessengerClient extends BaseClient {

	private Messenger mService = null;
	private boolean mBound = false;
	
	private Messenger mMessenger = null;
	
	private ServiceConnection mConnection = new ServiceConnection() {
		@Override
		public void onServiceDisconnected(ComponentName name) {
			mService = null;
			mBound = false;
		}
		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			mMessenger = new Messenger(getHandler());
			mService = new Messenger(service);
			try {
				Message msg = Message.obtain(null, AceStreamEngineMessages.MSG_REGISTER_CLIENT);
				msg.replyTo = mMessenger;
				mService.send(msg);

				msg = Message.obtain(null, AceStreamEngineMessages.MSG_START);
				mService.send(msg);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}
	};
	
	public MessengerClient(Context context) {
		super(context);
	}

	@Override
	public void bind() {
		if(!mBound) {
			Intent intent = new Intent();
			intent.setClassName("org.acestream.engine", "org.acestream.engine.service.AceStreamEngineService");
			mBound = getContext().bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
			if(!mBound) {
				Toast.makeText(getContext(), "Failed to bind", Toast.LENGTH_SHORT).show();
			}
		}
	}

	@Override
	public void unbind() {
		if(mBound) {
			if(mService != null) {
				Message msg = Message.obtain(null, AceStreamEngineMessages.MSG_UNREGISTER_CLIENT);
				msg.replyTo = mMessenger;
				try {
					mService.send(msg);
				} catch (RemoteException e) {
					e.printStackTrace();
				}
			}
			getContext().unbindService(mConnection);
			mBound = false;
		}
	}
}
