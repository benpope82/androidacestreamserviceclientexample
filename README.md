Android AceStream Service client example
----------------------------------------

This sample application demonstrates, how client can bind to AceStreamEngineService. There are two ways to bind to service: 

  - **Using AIDL**:
  To bind to service you need to use intent with action named the same as IAceStreamEngine.class and to receive callback from service you need IAceStreamEngineCallback implementation. Example of realisation you can find in AIDLClient.

  - **Using Messenger**: 
  To bind to service you need to use explicit name of AceStreamEngineService and use Messenger to communicate with it. MessengerClient demonstrates the simple way for using this method.

Download latest service interfaces you can [here].

Note: To build the application you need to add the android-support-v4 library to the project.

[here]: https://bitbucket.org/usvirid/androidacestreamserviceclientexample/downloads/org.acestream.engine.service.v0.zip
